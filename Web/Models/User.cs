﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System;

namespace Web.Models
{
    public enum Role
    {
        Guest = 1,
        User = 2,
        Admin = 3,
        SuperAdmin = 4,
    }

    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        string Name { get; set; }
        [EnumDataType(typeof(Role))]
        public int Role { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Address { get; set; }
        public ICollection<Order> Orders { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
